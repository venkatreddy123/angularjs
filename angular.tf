

resource "aws_security_group" "my_sg" {
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "8080"
        to_port     = "8080"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}
resource "aws_instance" "angularjs" {
    ami                         =  var.jbos
    instance_type               = "t2.large"
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.jbos.public_ip
    }

     provisioner "remote-exec" {
    inline = [
"apt-get update -y",
"sudo apt-get install python-software-properties",
"curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -",
"sudo apt-get install -y nodejs ",
"sudo echo Y |sudo npm install -g @angular/cli",
]
  }
   tags = {
    Name = "Nodejs"
  }
}